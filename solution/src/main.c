#include "bmp.h"
#include "rotate.h"
#include <stdio.h>

int main(int argc, char** argv) {

    if (argc < 4) {
        fprintf(stderr, "Usage: <input_image> <output_image> <custom_angle>\n");
        return EXIT_FAILURE;
    }

    char *endptr = NULL;
    int64_t custom_angle = strtoll(argv[3], &endptr, 10);

    FILE *input_image = fopen(argv[1], "rb");

    if (!input_image) {
        fprintf(stderr, "Ошибка открытия файла '%s'\n", argv[1]);
        return EXIT_FAILURE;
    }

    struct custom_image img = {0};
    enum read_result read = from_bmp(input_image, &img);

    if (read == READ_ERROR_MEMORY_ALLOCATION) {
        fprintf(stderr, "Ошибка памяти\n");
    } else if (read == READ_ERROR_INVALID_SIGNATURE) {
        fprintf(stderr, "Неверная подпись BMP\n");
    } else if (read == READ_ERROR_INVALID_BIT_DEPTH) {
        fprintf(stderr, "Неверное количество бит\n");
    } else if (read == READ_ERROR_CORRUPT_HEADER) {
        fprintf(stderr, "Поврежденный заголовок\n");
    } else {
        fprintf(stderr, "Неизвестная ошибка\n");
    }


    fclose(input_image);


    rotate(&img, custom_angle);

    char *output_image_path = argv[2];
    FILE *output_image = fopen(output_image_path, "wb");
    if (!output_image) {
        fprintf(stderr, "Ошибка открытия файла '%s' для записи\n", output_image_path);
        destroyImage(&img);
        return EXIT_FAILURE;
    }

    enum file_write_status fileWriteStatus = to_bmp(output_image, &img);
    fclose(output_image);

    destroyImage(&img);

    if (fileWriteStatus != SUCCESS) {
        fprintf(stderr, "Во время записи произошла ошибка\n");
        return EXIT_FAILURE;
    }

    printf("Сделанно\n");
    return EXIT_SUCCESS;
}
