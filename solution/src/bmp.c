#include "bmp_headers.h"

static uint64_t calculate_padding(uint64_t width) {
    uint64_t pixelsRowSize = width * sizeof(struct custom_pixel);
    uint64_t remainder = pixelsRowSize % 4;
    uint64_t rowPadding = remainder ? 4 - remainder : 0;
    return rowPadding;
}


enum read_result from_bmp(FILE* pFile, struct custom_image* pImage) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, pFile) != 1) {
        return READ_ERROR_INVALID_BIT_DEPTH;
    } else if (header.bfType != CUSTOM_BMP_SIGNATURE) {
        destroyImage(pImage);
        return READ_ERROR_INVALID_SIGNATURE;
    } else if (header.biSize != CUSTOM_HEADER_SIZE || header.biBitCount != CUSTOM_HEADER_BIT_COUNT) {
        destroyImage(pImage);
        return READ_ERROR_CORRUPT_HEADER;
    }

    *pImage = create_image(header.biWidth, header.biHeight);

    if (fseek(pFile, header.bOffBits, SEEK_SET) != 0) {
        destroyImage(pImage);
        return READ_ERROR_INVALID_BIT_DEPTH;
    }

    size_t padding = calculate_padding(pImage->width);
    for (size_t row = 0; row < pImage->height; ++row) {
        if (fread(pImage->data + row * pImage->width, sizeof(struct custom_pixel), pImage->width, pFile) != pImage->width) {
            destroyImage(pImage);
            return READ_ERROR_INVALID_BIT_DEPTH;
        }
        if (padding > 0 && fseek(pFile, (long)padding, SEEK_CUR) != 0) {
            destroyImage(pImage);
            return READ_ERROR_INVALID_BIT_DEPTH;
        }
    }


    return READ_SUCCESS;
}


enum file_write_status to_bmp(FILE* outputFile, struct custom_image const* img) {

    size_t padding = calculate_padding(img->width);
    size_t pixel_data_size = img->width * img->height * sizeof(struct custom_pixel);
    size_t total_img_size = sizeof(struct bmp_header) + pixel_data_size + img->height * padding;
    struct bmp_header header = {
            .bfType = CUSTOM_BMP_SIGNATURE,
            .bfileSize = total_img_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = CUSTOM_HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = CUSTOM_HEADER_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = pixel_data_size + img->height * padding, // Total size of the image data including padding
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };


    size_t write_header = fwrite(&header, sizeof(struct bmp_header), 1, outputFile);
    if (write_header != 1) {
        return ERROR_OCCURRED;
    }


    uint8_t padding_buffer[3] = {0};

    for (size_t row = 0; row < img->height; ++row) {
        size_t write_pixels = fwrite(img->data + row * img->width, sizeof(struct custom_pixel), img->width, outputFile);
        size_t padding_written = fwrite(padding_buffer, 1, padding, outputFile);

        if (write_pixels != img->width || padding_written != padding) {
            return ERROR_OCCURRED;
        }
    }

    return SUCCESS;
}
