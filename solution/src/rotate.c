#include "rotate.h"

static void transpose(struct custom_image* img) {
    for (uint64_t row = 0; row < img->height; row++) {
        for (uint64_t col = 0; col < img->width / 2; col++) {
            struct custom_pixel temp = img->data[row * img->width + col];
            img->data[row * img->width + col] = img->data[row * img->width + (img->width - col - 1)];
            img->data[row * img->width + (img->width - col - 1)] = temp;
        }
    }
}


static void transform_image(struct custom_image* img) {

    struct custom_pixel* new_data = malloc(img->width * img->height * sizeof(struct custom_pixel));
    if (!new_data) {
        fprintf(stderr, "Failed to allocate memory for transformed custom_image\n");
        return;
    }

    for (uint64_t index = 0; index < img->height; index++) {
        for (uint64_t col = 0; col < img->width; col++) {

            new_data[col * img->height + index] = img->data[index * img->width + col];
        }
    }


    destroyImage(img);


    img->data = new_data;

    uint64_t temp = img->width;
    img->width = img->height;
    img->height = temp;
}


enum rotation_result rotate(struct custom_image* image, int64_t custom_angle) {

    custom_angle = (360 - custom_angle) % 360 / 90;


    for (int64_t i = 0; i < custom_angle; i++) {

        transform_image(image);

        transpose(image);
    }

    return ROTATION_SUCCESS;
}

