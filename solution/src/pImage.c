#include "pImage.h"

struct custom_image create_image(uint64_t w, uint64_t h) {
    struct custom_image img = {0};

    img.data = calloc(w * h, sizeof(*img.data));
    if (img.data == NULL) {
        return img;
    }
    img.width = w;
    img.height = h;

    return img;
}

void destroyImage(struct custom_image* structcustomImage) {
    if (structcustomImage && structcustomImage->data) {
        free(structcustomImage->data);
        structcustomImage->data = NULL;
    }
}
