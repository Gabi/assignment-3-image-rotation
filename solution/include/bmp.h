#include <stdio.h>

struct custom_image;

enum read_result {
    READ_SUCCESS = 0,
    READ_ERROR_INVALID_SIGNATURE,
    READ_ERROR_INVALID_BIT_DEPTH,
    READ_ERROR_CORRUPT_HEADER,
    READ_ERROR_MEMORY_ALLOCATION
};

enum file_write_status {
    SUCCESS = 0,
    ERROR_OCCURRED
};

enum read_result from_bmp(FILE* pFile, struct custom_image* pImage);
enum file_write_status to_bmp(FILE* out, struct custom_image const* img);
