#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) custom_pixel {
    uint8_t blue, green, red;
};

struct __attribute__((packed)) custom_image {
    uint64_t width, height;
    struct custom_pixel* data;
};

struct custom_image create_image(size_t width, size_t height);

void destroyImage(struct custom_image* structcustomImage);
