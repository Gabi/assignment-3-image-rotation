#include <pImage.h>

enum rotation_result {
    ROTATION_SUCCESS,
};

enum rotation_result rotate(struct custom_image* image, int64_t custom_angle);
